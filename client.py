import socket
import random

HEADER = 10
PORT = 5050
FORMAT = 'utf-8'
SERVER = socket.gethostbyname(socket.gethostname())
ADDR = (SERVER, PORT)

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect(ADDR)


# actual code
def send(msg):
    message = msg.encode(FORMAT)
    msg_length = len(message)
    send_length = str(msg_length).encode(FORMAT)
    send_length += b' ' * (HEADER - len(send_length))
    client.send(send_length)
    client.send(message)
    return client.recv(2048).decode(FORMAT)


connect = True
print("You may use BYE to close the connection at any time.")
user_input = input("Enter username: ")
while connect:
    server_msg = send(user_input)
    print(server_msg)
    if server_msg == "ERR" or server_msg == "DONE" or server_msg == "END":
        connect = False
    else:
        user_input = input()
        if user_input.upper() == "BYE":
            connect = False


# disconnect
print(send("BYE"))
